/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.dao;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import ufps.entity.Persona;
import ufps.persistencia.PersonaJpaController;
import ufps.persistencia.exceptions.NonexistentEntityException;

/**
 *
 * @author Andrey R
 */
public class PersonaDAO {
    
    PersonaJpaController per=new PersonaJpaController();
    
    public void create(Persona p){
    
        try {
            per.create(p);
        } catch (Exception ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }
    
    public List<Persona> read(){
    return per.findPersonaEntities();
    }
    
    public void update(Persona p){
    
        try {
            per.edit(p);
        } catch (Exception ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }
    
    public void delete(Integer cedula){
        try {
            per.destroy(cedula);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(PersonaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public Persona readPersona(Integer cedula){
    return per.findPersona(cedula);
    }
    
}
