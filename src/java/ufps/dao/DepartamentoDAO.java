/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.dao;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import ufps.entity.Departamento;
import ufps.persistencia.DepartamentoJpaController;
import ufps.persistencia.exceptions.NonexistentEntityException;

/**
 *
 * @author Andrey R
 */
public class DepartamentoDAO {
    DepartamentoJpaController depar= new DepartamentoJpaController();
    

    
    public void create(Departamento p){
    
        try {
            depar.create(p);
        } catch (Exception ex) {
            Logger.getLogger(DepartamentoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }
    
    public List<Departamento> read(){
    return depar.findDepartamentoEntities();
    }
    
    public void update(Departamento p){
    
        try {
            depar.edit(p);
        } catch (Exception ex) {
            Logger.getLogger(DepartamentoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }
    
    public void delete(Integer idDpto){
        try {
            depar.destroy(idDpto);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(DepartamentoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public Departamento readMunicipios(Integer idMunicipio){
    return depar.findDepartamento(idMunicipio);
    }

}
    

