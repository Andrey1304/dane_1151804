/*
 * To change this license header, choose License Headers in Project Promunities.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.dao;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import ufps.entity.Municipios;
import ufps.persistencia.MunicipiosJpaController;
import ufps.persistencia.exceptions.NonexistentEntityException;


/**
 *
 * @author Andrey R
 */
public class MunicipiosDAO {
    
MunicipiosJpaController muni=new MunicipiosJpaController();
    
    public void create(Municipios p){
    
        try {
            muni.create(p);
        } catch (Exception ex) {
            Logger.getLogger(MunicipiosDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }
    
    public List<Municipios> read(){
    return muni.findMunicipiosEntities();
    }
    
    public void update(Municipios p){
    
        try {
            muni.edit(p);
        } catch (Exception ex) {
            Logger.getLogger(MunicipiosDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }
    
    public void delete(Integer idMunicipio){
        try {
            muni.destroy(idMunicipio);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(MunicipiosDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public Municipios readMunicipios(Integer idMunicipio){
    return muni.findMunicipios(idMunicipio);
    }

}
